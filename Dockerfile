FROM node:14 AS js
COPY js js
WORKDIR /js
RUN apt-get update -y && \
    apt-get install -y bsdmainutils && \
    yarn install && \
    yarn build

FROM ubuntu:20.04 AS binary
RUN apt-get update -y && apt-get install -y build-essential yasm
COPY --from=js index_html.asm .
COPY main.asm .
RUN yasm -f elf64 -a x86 main.asm -o main.o && \
    ld main.o -o asmnames && \
    strip --strip-all --discard-all asmnames && \
    strip \
			--remove-section=.eh_frame \
			--remove-section=.gnu.version \
			--remove-section=.hash \
			asmnames

FROM scratch AS release
COPY --from=binary asmnames .
ENTRYPOINT ["/asmnames"]
EXPOSE 8888

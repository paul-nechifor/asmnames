# ASM Names

Codenames HTTP in Assembly.

## Quick guide

Build everything and start with:

    docker-compose up

Then go to <http://localhost:8888>.

## Notes

Syscalls: <http://blog.rchapman.org/posts/Linux_System_Call_Table_for_x86_64/>

Instruction docs: https://www.felixcloutier.com/x86/jmp

Calling conventions:

* user : rdi, rsi, rdx, rcx, r8, r9 (System V AMD64 ABI)
*  linux: rdi, rsi, rdx, r10, r8, r9

When using `syscall`, the result is in `rax` and `rcx` and `r11` are clobbered.

* rbp - base of the current stack frame
* rsp - current position in the stack

Debuging:

    strace ./asmnames

Turn on core dump:

    ulimit -c unlimited

GDB:

    gdb -tui asmnames core

In gdb, show registers:

    layout regs

Print value at mem location:

    x 0x7f0dcf0b1000

Make a request:

    curl http://localhost:8888/b -so- | hexdump

Join 4 players:

    curl http://localhost:8888/a/0 -so-| hexdump -C
    curl http://localhost:8888/a/1 -so-| hexdump -C
    curl http://localhost:8888/a/2 -so-| hexdump -C
    curl http://localhost:8888/a/3 -so-| hexdump -C
    curl http://localhost:8888/b -so- | hexdump -C
    curl http://localhost:8888/c/1/word -so- | hexdump -C
    curl http://localhost:8888/b -so- | hexdump -C

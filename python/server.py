#!/usr/bin/env python3




import socket
import random
from copy import deepcopy
HOST, PORT = '127.0.0.1', 8888

n_words = 187

state = {
    "playersJoined": 0,
    "leftToPick": 0,

    # write state
    "players": [
        {
            "name": "",
        }
    for x in range(4)],
    "board": [
        [255, 255] # word number, card type
        for x in range(25)
    ],
    "mover": 255, # 0-red, 1-blue, 255-none
    "currentWord": "",
    "currentWordCount": 0,
}

def pad(w, n):
    w = w.encode('utf8')
    return (w + (b' ' * (n - len(w))))[:n]


def get_state():
    ret = []

    for player in state["players"]:
        ret.append(pad(player["name"], 10))

    for piece in state["board"]:
        ret.append(bytes([piece[0]]))
        ret.append(bytes([piece[1]]))

    ret.append(bytes([state["mover"]]))
    ret.append(pad(state["currentWord"], 20))
    ret.append(bytes([state["currentWordCount"]]))

    return b''.join(ret)


if __name__ == "__main__":
    my_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    my_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
    my_socket.bind((HOST, PORT))
    my_socket.listen(1)
    while True:
        connection, address = my_socket.accept()
        req = connection.recv(8 * 1024).decode('utf-8')
        path = req[4:].split(" ")[0] + "             "
        # print("path", path)
        # print(req)


        def send_text(arg, binary=True):
            string = (
                ("HTTP/1.0 200 OK\r\nContent-Type: application/x-binary\r\n"
                if binary else
                "HTTP/1.0 200 OK\r\nContent-Type: text/html\r\n") +
                "Server: asmnames 0.1.0"
                "\r\n\r\n"
            ).encode("utf8")
            connection.send(string + (arg.encode('utf*') if isinstance(arg, str) else arg))

        if path[1] == " ": # path is "/"
            send_text(
                "<!doctype html><meta charset='UTF-8'>"
                "<div id='root'></div>"
                f"<style>{open('../js/dist.css').read()}</style>"
                f"<script>{open('../js/dist.js').read()}</script>"
                , binary=False
            )
        elif path[1] == "a": # join as something
            which = ord(path[3]) - ord('0')
            if state["players"][which]["name"]:
                send_text(chr(0))
            else:
                print("joined as ", which)
                state["players"][which]["name"] = f"player{which}"
                send_text(chr(1))
                state["playersJoined"] += 1

                if state["playersJoined"] == 4: # set up the board
                    words = list(range(n_words))
                    random.shuffle(words)
                    for i, w in enumerate(words[:25]):
                        state["board"][i][0] = w
                    state["mover"] = 0

        elif path[1] == "b": # get the state
            which = ord(path[3]) - ord('0')
            send_text(get_state())
        elif path[1] == "c": # get the state
            state["currentWordCount"] = ord(path[3]) - ord('0')
            state["currentWord"] = path[5:].split(" ", 1)[0]
            state["leftToPick"] = state["currentWordCount"] + 1
            send_text(chr(1))
        else:
            connection.send(
                (
                    "HTTP/1.0 404 for oh four\r\nContent-Type: text/html\r\n"
                    "Server: asmnames 0.1.0"
                    "\r\n\r\n<!doctype html><meta charset='UTF-8'>"
                    "<h1>oops</h1>"
                ).encode('utf8')
            )

        connection.close()


"""


{
    "players": [
        "name", # times 4, max 10 characters, terminated by \0
    ]
    "cards": [
        "cardName",
        ""
    ] 
    "mover": # 0-red, 1-blue, 2-none
    "currentWord": # \0 means no word
    "currentWordCount": 0-nothing, 1-9-something
}

"""

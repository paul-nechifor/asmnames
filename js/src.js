// Preact
!function(){var n,l,u,t,i,o,r,f={},e=[],c=/acit|ex(?:s|g|n|p|$)|rph|grid|ows|mnc|ntw|ine[ch]|zoo|^ord|itera/i;function a(n,l){for(var u in l)n[u]=l[u];return n}function s(n){var l=n.parentNode;l&&l.removeChild(n)}function h(n,l,u){var t,i,o,r=arguments,f={};for(o in l)"key"==o?t=l[o]:"ref"==o?i=l[o]:f[o]=l[o];if(arguments.length>3)for(u=[u],o=3;o<arguments.length;o++)u.push(r[o]);if(null!=u&&(f.children=u),"function"==typeof n&&null!=n.defaultProps)for(o in n.defaultProps)void 0===f[o]&&(f[o]=n.defaultProps[o]);return v(n,f,t,i,null)}function v(l,u,t,i,o){var r={type:l,props:u,key:t,ref:i,__k:null,__:null,__b:0,__e:null,__d:void 0,__c:null,__h:null,constructor:void 0,__v:null==o?++n.__v:o};return null!=n.vnode&&n.vnode(r),r}function y(n){return n.children}function d(n,l){this.props=n,this.context=l}function p(n,l){if(null==l)return n.__?p(n.__,n.__.__k.indexOf(n)+1):null;for(var u;l<n.__k.length;l++)if(null!=(u=n.__k[l])&&null!=u.__e)return u.__e;return"function"==typeof n.type?p(n):null}function _(n){var l,u;if(null!=(n=n.__)&&null!=n.__c){for(n.__e=n.__c.base=null,l=0;l<n.__k.length;l++)if(null!=(u=n.__k[l])&&null!=u.__e){n.__e=n.__c.base=u.__e;break}return _(n)}}function m(l){(!l.__d&&(l.__d=!0)&&u.push(l)&&!k.__r++||i!==n.debounceRendering)&&((i=n.debounceRendering)||t)(k)}function k(){for(var n;k.__r=u.length;)n=u.sort(function(n,l){return n.__v.__b-l.__v.__b}),u=[],n.some(function(n){var l,u,t,i,o,r;n.__d&&(o=(i=(l=n).__v).__e,(r=l.__P)&&(u=[],(t=a({},i)).__v=i.__v+1,$(r,i,t,l.__n,void 0!==r.ownerSVGElement,null!=i.__h?[o]:null,u,null==o?p(i):o,i.__h),H(u,i),i.__e!=o&&_(i)))})}function b(n,l,u,t,i,o,r,c,a,s){var h,d,_,m,k,b,C,x=t&&t.__k||e,A=x.length;for(u.__k=[],h=0;h<l.length;h++)if(null!=(m=u.__k[h]=null==(m=l[h])||"boolean"==typeof m?null:"string"==typeof m||"number"==typeof m||"bigint"==typeof m?v(null,m,null,null,m):Array.isArray(m)?v(y,{children:m},null,null,null):m.__b>0?v(m.type,m.props,m.key,null,m.__v):m)){if(m.__=u,m.__b=u.__b+1,null===(_=x[h])||_&&m.key==_.key&&m.type===_.type)x[h]=void 0;else for(d=0;d<A;d++){if((_=x[d])&&m.key==_.key&&m.type===_.type){x[d]=void 0;break}_=null}$(n,m,_=_||f,i,o,r,c,a,s),k=m.__e,(d=m.ref)&&_.ref!=d&&(C||(C=[]),_.ref&&C.push(_.ref,null,m),C.push(d,m.__c||k,m)),null!=k?(null==b&&(b=k),"function"==typeof m.type&&null!=m.__k&&m.__k===_.__k?m.__d=a=g(m,a,n):a=w(n,m,_,x,k,a),s||"option"!==u.type?"function"==typeof u.type&&(u.__d=a):n.value=""):a&&_.__e==a&&a.parentNode!=n&&(a=p(_))}for(u.__e=b,h=A;h--;)null!=x[h]&&("function"==typeof u.type&&null!=x[h].__e&&x[h].__e==u.__d&&(u.__d=p(t,h+1)),j(x[h],x[h]));if(C)for(h=0;h<C.length;h++)T(C[h],C[++h],C[++h])}function g(n,l,u){var t,i;for(t=0;t<n.__k.length;t++)(i=n.__k[t])&&(i.__=n,l="function"==typeof i.type?g(i,l,u):w(u,i,i,n.__k,i.__e,l));return l}function w(n,l,u,t,i,o){var r,f,e;if(void 0!==l.__d)r=l.__d,l.__d=void 0;else if(null==u||i!=o||null==i.parentNode)n:if(null==o||o.parentNode!==n)n.appendChild(i),r=null;else{for(f=o,e=0;(f=f.nextSibling)&&e<t.length;e+=2)if(f==i)break n;n.insertBefore(i,o),r=o}return void 0!==r?r:i.nextSibling}function C(n,l,u,t,i){var o;for(o in u)"children"===o||"key"===o||o in l||A(n,o,null,u[o],t);for(o in l)i&&"function"!=typeof l[o]||"children"===o||"key"===o||"value"===o||"checked"===o||u[o]===l[o]||A(n,o,l[o],u[o],t)}function x(n,l,u){"-"===l[0]?n.setProperty(l,u):n[l]=null==u?"":"number"!=typeof u||c.test(l)?u:u+"px"}function A(n,l,u,t,i){var o;n:if("style"===l)if("string"==typeof u)n.style.cssText=u;else{if("string"==typeof t&&(n.style.cssText=t=""),t)for(l in t)u&&l in u||x(n.style,l,"");if(u)for(l in u)t&&u[l]===t[l]||x(n.style,l,u[l])}else if("o"===l[0]&&"n"===l[1])o=l!==(l=l.replace(/Capture$/,"")),l=l.toLowerCase()in n?l.toLowerCase().slice(2):l.slice(2),n.l||(n.l={}),n.l[l+o]=u,u?t||n.addEventListener(l,o?E:P,o):n.removeEventListener(l,o?E:P,o);else if("dangerouslySetInnerHTML"!==l){if(i)l=l.replace(/xlink[H:h]/,"h").replace(/sName$/,"s");else if("href"!==l&&"list"!==l&&"form"!==l&&"tabIndex"!==l&&"download"!==l&&l in n)try{n[l]=null==u?"":u;break n}catch(n){}"function"==typeof u||(null!=u&&(!1!==u||"a"===l[0]&&"r"===l[1])?n.setAttribute(l,u):n.removeAttribute(l))}}function P(l){this.l[l.type+!1](n.event?n.event(l):l)}function E(l){this.l[l.type+!0](n.event?n.event(l):l)}function $(l,u,t,i,o,r,f,e,c){var s,h,v,p,_,m,k,g,w,C,x,A=u.type;if(void 0!==u.constructor)return null;null!=t.__h&&(c=t.__h,e=u.__e=t.__e,u.__h=null,r=[e]),(s=n.__b)&&s(u);try{n:if("function"==typeof A){if(g=u.props,w=(s=A.contextType)&&i[s.__c],C=s?w?w.props.value:s.__:i,t.__c?k=(h=u.__c=t.__c).__=h.__E:("prototype"in A&&A.prototype.render?u.__c=h=new A(g,C):(u.__c=h=new d(g,C),h.constructor=A,h.render=z),w&&w.sub(h),h.props=g,h.state||(h.state={}),h.context=C,h.__n=i,v=h.__d=!0,h.__h=[]),null==h.__s&&(h.__s=h.state),null!=A.getDerivedStateFromProps&&(h.__s==h.state&&(h.__s=a({},h.__s)),a(h.__s,A.getDerivedStateFromProps(g,h.__s))),p=h.props,_=h.state,v)null==A.getDerivedStateFromProps&&null!=h.componentWillMount&&h.componentWillMount(),null!=h.componentDidMount&&h.__h.push(h.componentDidMount);else{if(null==A.getDerivedStateFromProps&&g!==p&&null!=h.componentWillReceiveProps&&h.componentWillReceiveProps(g,C),!h.__e&&null!=h.shouldComponentUpdate&&!1===h.shouldComponentUpdate(g,h.__s,C)||u.__v===t.__v){h.props=g,h.state=h.__s,u.__v!==t.__v&&(h.__d=!1),h.__v=u,u.__e=t.__e,u.__k=t.__k,u.__k.forEach(function(n){n&&(n.__=u)}),h.__h.length&&f.push(h);break n}null!=h.componentWillUpdate&&h.componentWillUpdate(g,h.__s,C),null!=h.componentDidUpdate&&h.__h.push(function(){h.componentDidUpdate(p,_,m)})}h.context=C,h.props=g,h.state=h.__s,(s=n.__r)&&s(u),h.__d=!1,h.__v=u,h.__P=l,s=h.render(h.props,h.state,h.context),h.state=h.__s,null!=h.getChildContext&&(i=a(a({},i),h.getChildContext())),v||null==h.getSnapshotBeforeUpdate||(m=h.getSnapshotBeforeUpdate(p,_)),x=null!=s&&s.type===y&&null==s.key?s.props.children:s,b(l,Array.isArray(x)?x:[x],u,t,i,o,r,f,e,c),h.base=u.__e,u.__h=null,h.__h.length&&f.push(h),k&&(h.__E=h.__=null),h.__e=!1}else null==r&&u.__v===t.__v?(u.__k=t.__k,u.__e=t.__e):u.__e=I(t.__e,u,t,i,o,r,f,c);(s=n.diffed)&&s(u)}catch(l){u.__v=null,(c||null!=r)&&(u.__e=e,u.__h=!!c,r[r.indexOf(e)]=null),n.__e(l,u,t)}}function H(l,u){n.__c&&n.__c(u,l),l.some(function(u){try{l=u.__h,u.__h=[],l.some(function(n){n.call(u)})}catch(l){n.__e(l,u.__v)}})}function I(n,l,u,t,i,o,r,c){var a,h,v,y,d=u.props,p=l.props,_=l.type,m=0;if("svg"===_&&(i=!0),null!=o)for(;m<o.length;m++)if((a=o[m])&&(a===n||(_?a.localName==_:3==a.nodeType))){n=a,o[m]=null;break}if(null==n){if(null===_)return document.createTextNode(p);n=i?document.createElementNS("http://www.w3.org/2000/svg",_):document.createElement(_,p.is&&p),o=null,c=!1}if(null===_)d===p||c&&n.data===p||(n.data=p);else{if(o=o&&e.slice.call(n.childNodes),h=(d=u.props||f).dangerouslySetInnerHTML,v=p.dangerouslySetInnerHTML,!c){if(null!=o)for(d={},y=0;y<n.attributes.length;y++)d[n.attributes[y].name]=n.attributes[y].value;(v||h)&&(v&&(h&&v.__html==h.__html||v.__html===n.innerHTML)||(n.innerHTML=v&&v.__html||""))}if(C(n,p,d,i,c),v)l.__k=[];else if(m=l.props.children,b(n,Array.isArray(m)?m:[m],l,u,t,i&&"foreignObject"!==_,o,r,n.firstChild,c),null!=o)for(m=o.length;m--;)null!=o[m]&&s(o[m]);c||("value"in p&&void 0!==(m=p.value)&&(m!==n.value||"progress"===_&&!m)&&A(n,"value",m,d.value,!1),"checked"in p&&void 0!==(m=p.checked)&&m!==n.checked&&A(n,"checked",m,d.checked,!1))}return n}function T(l,u,t){try{"function"==typeof l?l(u):l.current=u}catch(l){n.__e(l,t)}}function j(l,u,t){var i,o,r;if(n.unmount&&n.unmount(l),(i=l.ref)&&(i.current&&i.current!==l.__e||T(i,null,u)),t||"function"==typeof l.type||(t=null!=(o=l.__e)),l.__e=l.__d=void 0,null!=(i=l.__c)){if(i.componentWillUnmount)try{i.componentWillUnmount()}catch(l){n.__e(l,u)}i.base=i.__P=null}if(i=l.__k)for(r=0;r<i.length;r++)i[r]&&j(i[r],u,t);null!=o&&s(o)}function z(n,l,u){return this.constructor(n,u)}function F(l,u,t){var i,o,r;n.__&&n.__(l,u),o=(i="function"==typeof t)?null:t&&t.__k||u.__k,r=[],$(u,l=(!i&&t||u).__k=h(y,null,[l]),o||f,f,void 0!==u.ownerSVGElement,!i&&t?[t]:o?null:u.firstChild?e.slice.call(u.childNodes):null,r,!i&&t?t:o?o.__e:u.firstChild,i),H(r,l)}n={__e:function(n,l){for(var u,t,i;l=l.__;)if((u=l.__c)&&!u.__)try{if((t=u.constructor)&&null!=t.getDerivedStateFromError&&(u.setState(t.getDerivedStateFromError(n)),i=u.__d),null!=u.componentDidCatch&&(u.componentDidCatch(n),i=u.__d),i)return u.__E=u}catch(l){n=l}throw n},__v:0},l=function(n){return null!=n&&void 0===n.constructor},d.prototype.setState=function(n,l){var u;u=null!=this.__s&&this.__s!==this.state?this.__s:this.__s=a({},this.state),"function"==typeof n&&(n=n(a({},u),this.props)),n&&a(u,n),null!=n&&this.__v&&(l&&this.__h.push(l),m(this))},d.prototype.forceUpdate=function(n){this.__v&&(this.__e=!0,n&&this.__h.push(n),m(this))},d.prototype.render=y,u=[],t="function"==typeof Promise?Promise.prototype.then.bind(Promise.resolve()):setTimeout,k.__r=0,o=0,r={render:F,hydrate:function n(l,u){F(l,u,n)},createElement:h,h:h,Fragment:y,createRef:function(){return{current:null}},isValidElement:l,Component:d,cloneElement:function(n,l,u){var t,i,o,r=arguments,f=a({},n.props);for(o in l)"key"==o?t=l[o]:"ref"==o?i=l[o]:f[o]=l[o];if(arguments.length>3)for(u=[u],o=3;o<arguments.length;o++)u.push(r[o]);return null!=u&&(f.children=u),v(n.type,f,t||n.key,i||n.ref,null)},createContext:function(n,l){var u={__c:l="__cC"+o++,__:n,Consumer:function(n,l){return n.children(l)},Provider:function(n){var u,t;return this.getChildContext||(u=[],(t={})[l]=this,this.getChildContext=function(){return t},this.shouldComponentUpdate=function(n){this.props.value!==n.value&&u.some(m)},this.sub=function(n){u.push(n);var l=n.componentWillUnmount;n.componentWillUnmount=function(){u.splice(u.indexOf(n),1),l&&l.call(n)}}),n.children}};return u.Provider.__=u.Consumer.contextType=u},toChildArray:function n(l,u){return u=u||[],null==l||"boolean"==typeof l||(Array.isArray(l)?l.some(function(l){n(l,u)}):u.push(l)),u},options:n},typeof module<"u"?module.exports=r:self.preact=r}();

// Hooks
var n,t,r,o=preact,u=0,i=[],c=o.options.__b,e=o.options.__r,f=o.options.diffed,a=o.options.__c,v=o.options.unmount;function p(n,r){o.options.__h&&o.options.__h(t,n,u||r),u=0;var i=t.__H||(t.__H={__:[],__h:[]});return n>=i.__.length&&i.__.push({}),i.__[n]}function s(n){return u=1,x(A,n)}function x(r,o,u){var i=p(n++,2);return i.t=r,i.__c||(i.__=[u?u(o):A(void 0,o),function(n){var t=i.t(i.__[0],n);i.__[0]!==t&&(i.__=[t,i.__[1]],i.__c.setState({}))}],i.__c=t),i.__}function m(r,u){var i=p(n++,4);!o.options.__s&&q(i.__H,u)&&(i.__=r,i.__H=u,t.__h.push(i))}function l(t,r){var o=p(n++,7);return q(o.__H,r)&&(o.__=t(),o.__H=r,o.__h=t),o.__}function y(){i.forEach(function(n){if(n.__P)try{n.__H.__h.forEach(_),n.__H.__h.forEach(d),n.__H.__h=[]}catch(t){n.__H.__h=[],o.options.__e(t,n.__v)}}),i=[]}o.options.__b=function(n){t=null,c&&c(n)},o.options.__r=function(r){e&&e(r),n=0;var o=(t=r.__c).__H;o&&(o.__h.forEach(_),o.__h.forEach(d),o.__h=[])},o.options.diffed=function(n){f&&f(n);var u=n.__c;u&&u.__H&&u.__H.__h.length&&(1!==i.push(u)&&r===o.options.requestAnimationFrame||((r=o.options.requestAnimationFrame)||function(n){var t,r=function(){clearTimeout(o),h&&cancelAnimationFrame(t),setTimeout(n)},o=setTimeout(r,100);h&&(t=requestAnimationFrame(r))})(y)),t=void 0},o.options.__c=function(n,t){t.some(function(n){try{n.__h.forEach(_),n.__h=n.__h.filter(function(n){return!n.__||d(n)})}catch(r){t.some(function(n){n.__h&&(n.__h=[])}),t=[],o.options.__e(r,n.__v)}}),a&&a(n,t)},o.options.unmount=function(n){v&&v(n);var t=n.__c;if(t&&t.__H)try{t.__H.__.forEach(_)}catch(n){o.options.__e(n,t.__v)}};var h="function"==typeof requestAnimationFrame;function _(n){var r=t;"function"==typeof n.__c&&n.__c(),t=r}function d(n){var r=t;n.__c=n.__(),t=r}function q(n,t){return!n||n.length!==t.length||t.some(function(t,r){return t!==n[r]})}function A(n,t){return"function"==typeof t?t(n):t}preact.useState=s,preact.useReducer=x,preact.useEffect=function(r,u){var i=p(n++,3);!o.options.__s&&q(i.__H,u)&&(i.__=r,i.__H=u,t.__H.__h.push(i))},preact.useLayoutEffect=m,preact.useRef=function(n){return u=5,l(function(){return{current:n}},[])},preact.useImperativeHandle=function(n,t,r){u=6,m(function(){"function"==typeof n?n(t()):n&&(n.current=t())},null==r?r:r.concat(n))},preact.useMemo=l,preact.useCallback=function(n,t){return u=8,l(function(){return n},t)},preact.useContext=function(r){var o=t.context[r.__c],u=p(n++,9);return u.__c=r,o?(null==u.__&&(u.__=!0,o.sub(t)),o.props.value):r.__},preact.useDebugValue=function(n,t){o.options.useDebugValue&&o.options.useDebugValue(t?t(n):n)},preact.useErrorBoundary=function(r){var o=p(n++,10),u=s();return o.__=r,t.componentDidCatch||(t.componentDidCatch=function(n){o.__&&o.__(n),u[1](n)}),[u[0],function(){u[1](void 0)}]};

const { h: html, render, useState } = preact;

/** @jsx html */

const words = [
  'africa',
  'agent',
  'air',
  'alien',
  'amazon',
  'angel',
  'antarctica',
  'apple',
  'arm',
  'back',
  'band',
  'bank',
  'bark',
  'beach',
  'belt',
  'berlin',
  'berry',
  'board',
  'bond',
  'boom',
  'bow',
  'box',
  'bug',
  'canada',
  'capital',
  'cell',
  'center',
  'china',
  'chocolate',
  'circle',
  'club',
  'compound',
  'copper',
  'crash',
  'cricket',
  'cross',
  'death',
  'dice',
  'dinosaur',
  'doctor',
  'dog',
  'dress',
  'dwarf',
  'eagle',
  'egypt',
  'engine',
  'england',
  'europe',
  'eye',
  'fair',
  'fall',
  'fan',
  'field',
  'file',
  'film',
  'fish',
  'flute',
  'fly',
  'forest',
  'fork',
  'france',
  'gas',
  'ghost',
  'giant',
  'glass',
  'glove',
  'gold',
  'grass',
  'greece',
  'green',
  'ham',
  'head',
  'himalaya',
  'hole',
  'hood',
  'hook',
  'human',
  'horseshoe',
  'hospital',
  'hotel',
  'ice',
  'ice cream',
  'india',
  'iron',
  'ivory',
  'jam',
  'jet',
  'jupiter',
  'kangaroo',
  'ketchup',
  'kid',
  'king',
  'kiwi',
  'knife',
  'knight',
  'lab',
  'lap',
  'laser',
  'lawyer',
  'lead',
  'lemon',
  'limousine',
  'leadlock',
  'log',
  'mammoth',
  'maple',
  'march',
  'mass',
  'mercury',
  'millionaire',
  'model',
  'mole',
  'moscow',
  'mouth',
  'mug',
  'needle',
  'net',
  'new york',
  'night',
  'note',
  'novel',
  'nurse',
  'nut',
  'oil',
  'olive',
  'olympus',
  'opera',
  'orange',
  'paper',
  'park',
  'part',
  'paste',
  'phoenix',
  'piano',
  'telescope',
  'teacher',
  'switch',
  'swing',
  'sub',
  'stick',
  'staff',
  'stadium',
  'sprint',
  'spike',
  'snowman',
  'slip',
  'shot',
  'shadow',
  'server',
  'ruler',
  'row',
  'rose',
  'root',
  'rome',
  'rock',
  'robot',
  'robin',
  'revolution',
  'rat',
  'racket',
  'queen',
  'press',
  'port',
  'pilot',
  'time',
  'tooth',
  'tower',
  'truck',
  'triangle',
  'trip',
  'turkey',
  'undertaker',
  'unicorn',
  'vacuum',
  'van',
  'wake',
  'wall',
  'war',
  'washer',
  'washington',
  'water',
  'wave',
  'well',
  'whale',
  'whip',
  'worm',
  'yard',
];

const playerIndex = {
  red: { spymaster: 0, operative: 1 },
  blue: { spymaster: 2, operative: 3},
};

function api(url) {
  return new Promise(resolve => {
    fetch(url).then(response => {
      response.arrayBuffer().then(buffer => {
        resolve(new Uint8Array(buffer));
      });
    });
  });
}

function Player({team, position, data, updateNow, meIndex, active}) {
  const index = playerIndex[team][position];

  const onClick = () => {
    api(`/a/${index}`).then(data => {
      if (data[0]) {
        updateNow(index);
      }
    });
  };

  return (
    <div className={`team ${active ? 'team-active' : ''}`}>
      {(meIndex >= 0) ? (
        data.name ? (
          (meIndex === index) ? (
            <div><strong>{data.name}</strong> ({position}, me)</div>
          ) : (
            <div>{data.name} ({position})</div>
          )
        ) : (
          <div>Waiting for {position}...</div>
        )
      ) : (
        data.name ? (
          <div>{data.name} ({position})</div>
        ) : (
          <button onClick={onClick}>Join as {position}</button>
        )
      )}
    </div>
  );
  ;
}

function Team({name, players, updateNow, meIndex}) {
  return (
    <div className={`team team-${name}`}>
      <div className="team-name">{name}</div>
      <Player team={name} position="spymaster" data={players.spymaster} updateNow={updateNow} meIndex={meIndex} />
      <Player team={name} position="operative" data={players.operative} updateNow={updateNow} meIndex={meIndex}/>
    </div>
  );
}

function range(n) {
  return [...new Array(n).keys()];
}

function updateField(setValue) {
  return ev => {
    setValue(ev.target.value);
  };
}

function readString(data, start, end) {
  return String.fromCharCode.apply(null, data.slice(start, end)).trim();
}

function NewWord({updateNow}) {
  const [word, setWord] = useState('');
  const [nWords, setNWords] = useState(1);
  const onWordChange = updateField(setWord);
  const onNWordsChange = updateField(setNWords);
  const onSubmit = () => {
    let n = parseInt(nWords, 10);
    n = (n >= 1 && n <= 9) ? n : 1;
    api(`/c/${n}/${word.trim().substring(0, 20)}`).then(() => updateNow());
  };
  return (
    <div>
      <input value={word} onChange={onWordChange} onKeyUp={onWordChange}/>
      <input value={nWords} onChange={onNWordsChange} onKeyUp={onNWordsChange}/>
      <button onClick={onSubmit}>Send</button>
    </div>
  );
}

function App() {
  const [counter, setCounter] = useState(0);
  const [meIndex, setMeIndex] = useState(-1);
  const [players, setPlayers] = useState({
    red: {
      spymaster: {
        name: '',
      },
      operative: {
        name: '',
      },
    },
    blue: {
      spymaster: {
        name: '',
      },
      operative: {
        name: '',
      },
    },
  });

  const [cards, setCards] = useState(range(25).map(
    x => ({
      word: '',
      type: 'unknown'
    }),
  ));

  const [mover, setMover] = useState(255);
  const [currentWord, setCurrentWord] = useState('');
  const [currentWordCount, setCurrentWordCount] = useState(0);

  function startLoop() {
    api("/b").then(data => {
      let k = 0;

      const players = {
        red: {
          spymaster: {
            name: readString(data, k, k + 10),
          },
          operative: {
            name: readString(data, k + 10, k + 20),
          },
        },
        blue: {
          spymaster: {
            name: readString(data, k + 20, k + 30),
          },
          operative: {
            name: readString(data, k + 30, k + 40),
          },
        },
      };
      k += 40;
      setPlayers(() => players);

      const cards = range(25).map(i => ({
        word: data[k + i * 2] === 255 ? '' : words[data[k + i * 2]],
        type: {0: 'red', 1: 'blue', 2: 'bystander', 3: 'black', 255: 'unknown'}[data[k + i * 2 + 1]],
      }));

      k += 2 * 25;

      setCards(cards);

      setMover(data[k]);
      k += 1;

      setCurrentWord(readString(data, k, k + 20))
      k += 20;

      setCurrentWordCount(data[k])
      k++;
    });

    setTimeout(startLoop, 1500);
  }

  // Only start when it doesn't exist.
  if (!document.querySelectorAll('nav').length) {
    setTimeout(startLoop, 0);
  }
  const updateNow = index => {
    setMeIndex(index);
    setTimeout(startLoop, 0);
  }

  return (
    <div className="root-columns">
      <nav>
        <h1>ASM Names</h1>
        <Team name="red" players={players.red} updateNow={updateNow} meIndex={meIndex} active={mover === 0}/>
        <Team name="blue" players={players.blue} updateNow={updateNow} meIndex={meIndex} active={mover === 1}/>
        <button>(╯°□°）╯︵ ┻━┻</button>
      </nav>
      <div className="main">
        <div className="cards">
          {cards.map(x => (
            <div className="card">
              <div className="card-name">{x.word}</div>
            </div>
          ))}
        </div>

        {(!currentWordCount && (mover < 2) && meIndex === (mover << 1)) && (
          <NewWord updateNow={startLoop}/>
        )}

        {!!currentWordCount && (
          <div className="current-word">
            <span>{currentWord}</span>
            {' '}
            <span>{currentWordCount}</span>
          </div>
        )}
      </div>
    </div>
  );
};

render(<App />, document.getElementById("root"));

import pytest
import requests
import subprocess
import os


def from_bytes(b):
    ret = {
        "players": [],
        "board": [],
        "mover": None,
        "currentWord": None,
        "currentWordCount": None,
    }

    k = 0

    for i in range(4):
        ret["players"].append(b[k:k+10].decode().strip())
        k += 10

    for i in range(25):
        ret["board"].append([int(b[k]), int(b[k + 1])])
        k += 2

    ret["mover"] = int(b[k])
    k += 1
    ret["currentWord"] = b[k:k+20].decode().strip()
    k += 20
    ret["currentWordCount"] = int(b[k])

    return ret


class State:
    def __init__(self):
        self.state = {
            "playersJoined": 0,
            "leftToPick": 0,

            # write state
            "players": [{"name": "" } for x in range(4)],
            "board": [
                [255, 255] # word number, card type
                for x in range(25)
            ],
            "mover": 255, # 0-red, 1-blue, 255-none
            "currentWord": "",
            "currentWordCount": 0,
        }

    def pad(self, w, n):
        w = w.encode('utf8')
        return (w + (b' ' * (n - len(w))))[:n]

    def to_bytes(self):
        ret = []

        for player in self.state["players"]:
            ret.append(self.pad(player["name"], 10))

        for piece in self.state["board"]:
            ret.append(bytes([piece[0]]))
            ret.append(bytes([piece[1]]))

        ret.append(bytes([self.state["mover"]]))
        ret.append(self.pad(self.state["currentWord"], 20))
        ret.append(bytes([self.state["currentWordCount"]]))

        return b''.join(ret)



@pytest.fixture
def asmnames():
    # 00004553207 is 1234567 in base 8
    process = subprocess.Popen(["strace", "../asmnames", "00004553207"])
    yield
    process.kill()
    os.system(r"""
        ps aux | grep '\.\.\/asmnames' | awk '{ print $2 }' |
        xargs kill -9 &>/dev/null|| true
    """)


def req(url):
    res = requests.get(f"http://localhost:8888{url}")
    return res.content


def test_happy_path(asmnames):
    # Get the initial state.
    assert from_bytes(req("/b")) == {
        "players": [''] * 4,
        "board": [[255, 255]] * 25,
        "mover": 255,
        "currentWord": '',
        "currentWordCount": 0,
    }

    # First player joins.
    assert req("/a/0") == b"\x01"
    assert from_bytes(req("/b"))["players"] == ["p0", '', '', '']

    assert req("/a/1") == b"\x01"
    assert from_bytes(req("/b"))["players"] == ["p0", 'p1', '', '']

    assert req("/a/2") == b"\x01"
    assert from_bytes(req("/b"))["players"] == ["p0", 'p1', 'p2', '']

    # Forth player joins, the cards are shuffled.
    assert req("/a/3") == b"\x01"
    assert from_bytes(req("/b")) == {
        "players": ["p0", 'p1', 'p2', 'p3'],
        "board": [
            [51, 255],
            [60, 255],
            [105, 255],
            [147, 255],
            [169, 255],
            [64, 255],
            [87, 255],
            [165, 255],
            [137, 255],
            [38, 255],
            [107, 255],
            [8, 255],
            [169, 255],
            [30, 255],
            [122, 255],
            [168, 255],
            [121, 255],
            [63, 255],
            [59, 255],
            [129, 255],
            [28, 255],
            [108, 255],
            [186, 255],
            [118, 255],
            [36, 255],
        ],
        "mover": 0,
        "currentWord": '',
        "currentWordCount": 0,
    }

    # Set the suggestion and count.
    assert req("/c/1/animal") == b"\x01"
    assert from_bytes(req("/b")) == {
        "players": ["p0", 'p1', 'p2', 'p3'],
        "board": [
            [51, 255],
            [60, 255],
            [105, 255],
            [147, 255],
            [169, 255],
            [64, 255],
            [87, 255],
            [165, 255],
            [137, 255],
            [38, 255],
            [107, 255],
            [8, 255],
            [169, 255],
            [30, 255],
            [122, 255],
            [168, 255],
            [121, 255],
            [63, 255],
            [59, 255],
            [129, 255],
            [28, 255],
            [108, 255],
            [186, 255],
            [118, 255],
            [36, 255],
        ],
        "mover": 0,
        "currentWord": 'animal',
        "currentWordCount": 1,
    }

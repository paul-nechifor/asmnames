; ## System defs
%define AF_INET 2
%define BUFFER_SIZE 8192
%define FD_STDOUT 1
%define LEVEL_SOL_TCP 1
%define MMAP_MAP_ANON 0x20
%define MMAP_MAP_PRIVATE 0x2
%define MMAP_PROT_READ 0x1
%define MMAP_PROT_WRITE 0x2
%define PROTO_TCP 6
%define SOCKOPT_TCP_REUSEADDR 2
%define SOCK_STREAM 1
%define SYS_ACCEPT 43
%define SYS_BIND 49
%define SYS_CLOSE 3
%define SYS_EXIT 60
%define SYS_LISTEN 50
%define SYS_MMAP 9
%define SYS_RECVFROM 45
%define SYS_SENDTO 44
%define SYS_SETSOCKOPT 54
%define SYS_SOCKET 41
%define SYS_TIME 201
%define SYS_WRITE 1

; ## Stack variables
;
; Every number represents an offset from the base. So, for example, to read
; `VAR_REQ_BUF` into `rax` you'd do:
;
;     mov rax, [rbp - VAR_REQ_BUF]
;
; `STACK_VAR_RESERVED_SIZE` represents the total space occupied.
%define VAR_SOCKET_FD 8
%define VAR_REQ_BUF 16
%define VAR_GAME_STATE 24
%define STACK_VAR_RESERVED_SIZE 24

; ## Various game constants
%define N_PLAYERS 4
%define PLAYER_NAME_LEN 10
%define N_CARDS 25
%define CARD_STATE_LENGTH 2
%define CURRENT_WORD_LEN 20
%define N_WORDS 187

; ## The game state struct
;
; This struct contains the entire state of the game. The first values are
; private, and everything between `sendable_state_start` and
; `sendable_state_end` is public. Everything between those two labels is sent
; in the state request (`/b`).
;
; The struct has a fixed length and doesn't grow. Strings (such as player names
; and words) have a fixed size. Unused characters are set to `' '` so that the
; frontend can get the string by using `.trim()`.
struc game_state
  ; The number of players which have joined. When it reaches `N_PLAYERS` it
  ; means the game can start.
  n_players_joined: resb 1
  sendable_state_start:
  ; `players` is an array of player names. An empty string (i.e. just spaces)
  ; means the player slot is free.
  players: resb (N_PLAYERS * PLAYER_NAME_LEN)
  ; `cards` is an array containing two 1-byte values:
  ;
  ; * the index of the word on the card (or 255 if no word)
  ; * the type of the card (TODO add detauils)
  cards: resb (N_CARDS * CARD_STATE_LENGTH)
  mover: resb 1
  current_word: resb CURRENT_WORD_LEN
  current_word_count: resb 1
  sendable_state_end:
endstruc

section .data
  struc sockaddr_in
    sin_family: resw 1
    sin_port:   resw 1
    sin_addr:   resd 1
  endstruc

  sa: istruc sockaddr_in
    at sin_family, dw AF_INET
    at sin_port,   dw 0
    at sin_addr,   dd 0 ;INADDR_ANY
  iend

  ; An error message which doesn't overwhelm the user with verbosity.
  msg_error db "uups...", 10, 0
  msg_error_len equ $ - msg_error

  ; The header for the index page. It's the only HTML endpoint.
  header_str db "HTTP/1.0 200 OK", 13, 10, "Content-Type: text/html", 13, 10, "Server: asmnames 0.1.0", 13, 10, 13, 10
  header_len equ $ - header_str

  ; The header for the API endpoints. They're all binary.
  header_binary_str db "HTTP/1.0 200 OK", 13, 10, "Content-Type: application/x-binary", 13, 10, "Server: asmnames 0.1.0", 13, 10, 13, 10
  header_binary_len equ $ - header_binary_str

  constant_one: dq 1 ; Pointer to 1.

  %include "index_html.asm"

section .bss
  listen_socket: resq 1
  constant_zero: resq 1 ; Pointer to 0.
  random_state: resq 1

section .text

sys_close:
  mov rax, SYS_CLOSE
  syscall
  ret

sys_mmap_mem: ; rdi: size
  push rsi
  push rdi
  push rdx
  push r10
  push r8
  push r9
  mov rsi, rdi ; Size
  xor rdi, rdi ; Preferred address (don't care)
  mov rdx, MMAP_PROT_READ | MMAP_PROT_WRITE ; Protection Flags
  mov r10, MMAP_MAP_PRIVATE | MMAP_MAP_ANON ; Flags
  xor r8, r8
  dec r8 ; -1 fd because of MMAP_MAP_ANON
  xor r9, r9 ; Offset
  mov rax, SYS_MMAP
  syscall
  pop r9
  pop r8
  pop r10
  pop rdx
  pop rdi
  pop rsi
  ret

sys_recvfrom:
  push r10
  push r8
  push r9
  xor r10, r10 ; flags
  xor r8, r8
  xor r9, r9
  mov rax, SYS_RECVFROM
  syscall
  pop r9
  pop r8
  pop r10
  ret

sys_sendto:
  push r10
  push r8
  push r9
  xor r10, r10
  xor r8, r8
  xor r9, r9
  mov rax, SYS_SENDTO
  syscall
  pop r9
  pop r8
  pop r10
  ret

; A random number generator function using the [linear congruential
; generator](https://en.wikipedia.org/wiki/Linear_congruential_generator)
; algorithm.
random:
  push rdx
  push rcx

  mov rax, [random_state]
  mov rcx, 48271
  mul rcx
  mov rcx, 0x7fffffff
  div rcx
  mov rax, rdx
  mov [random_state], rax

  pop rcx
  pop rdx
  ret

; ## The start of the program
global _start
_start:

  ; Arguments are passed to the executable through `rsp`:
  ;
  ; * `argc = [rsp]`
  ; * `argv = [rsp + 8 * arg_number]`
  ;
  ; Note that the first arg is the program name, so `argc` is always at least 1.

  ; Read the number of arguments.
  mov rdi, [rsp]

  ; If no arguments, do the time based initialization.
  cmp rdi, 1
  je init_random_with_time

  ; The first argument is the seed which  needs to be sent as 11 digits in base
  ; 8 (this is to make it easy to read).

  ; * `rbx` is the pointer to the string containing the number
  ; * `rax` is where the number will be formed.
  mov rbx, [rsp + 16]
  xor rax, rax
  xor rdx, rdx
  mov rcx, 11

next_seed_digit:
  mov dl, [rbx]
  sub rdx, 48
  or rax, rdx
  dec rcx
  jz got_the_seed
  shl rax, 3
  inc rbx
  jmp next_seed_digit

init_random_with_time:
  ; Init the random number generator.
  mov rax, SYS_TIME
  xor rdi, rdi
  syscall

got_the_seed:
  and rax, 0x7fffffff
  mov [random_state], rax

  ; Create the socket.
  mov rdi, AF_INET
  mov rsi, SOCK_STREAM
  mov rdx, PROTO_TCP
  mov rax, SYS_SOCKET
  syscall
  cmp rax, 0
  jl exit_error
  mov [listen_socket], rax

  ; Reuse the address.
  mov rdi, [listen_socket]
  mov rsi, LEVEL_SOL_TCP
  mov rdx, SOCKOPT_TCP_REUSEADDR
  mov r10, constant_one ; Pointer to 1.
  mov r8, 8 ; sizeof int
  mov rax, SYS_SETSOCKOPT
  syscall

  ; Bind to port.
  ; 47138 is 8888 in network byte order. This is equivalent to:
  ;     mov rax, 8888
  ;     xchg al, ah
  mov rax, 47138
  mov [sa + sin_port], ax
  mov rdi, [listen_socket]
  mov rsi, sa
  mov rdx, 16
  mov rax, SYS_BIND
  syscall
  cmp rax, 0
  jl exit_error

  ; Start listening.
  mov rdi, [listen_socket]
  mov rsi, 100000000 ; Connection queue lenght.
  mov rax, SYS_LISTEN
  syscall

  ; Make room on the stack for all the stack variables.
  mov rbp, rsp
  sub rsp, STACK_VAR_RESERVED_SIZE

  ; Allocate space for the socket read buffer.
  mov rdi, BUFFER_SIZE
  call sys_mmap_mem
  mov [rbp - VAR_REQ_BUF], rax

  ; Allocate space for the game state
  mov rdi, game_state_size
  call sys_mmap_mem
  mov [rbp - VAR_GAME_STATE], rax

  ; Initialise state
  mov [rax], BYTE 0 ; Initialise n_players_joined.
  inc rax
  mov rcx, (N_PLAYERS * PLAYER_NAME_LEN)
init_players:
  mov [rax], BYTE 32 ; ' '
  inc rax
  dec rcx
  jz init_cards_start
  jmp init_players
init_cards_start:
  mov rcx, (N_CARDS * CARD_STATE_LENGTH)
init_cards:
  mov [rax], BYTE 255
  inc rax
  dec rcx
  jz init_mover
  jmp init_cards
init_mover:
  mov [rax], BYTE 255
  inc rax
  mov rcx, CURRENT_WORD_LEN
init_current_word:
  mov [rax], BYTE 32 ; ' '
  inc rax
  dec rcx
  jz init_current_word_count
  jmp init_current_word
init_current_word_count:
  mov [rax], BYTE 0

new_connection:
  mov rdi, [listen_socket]
  xor rsi, rsi
  xor rdx, rdx
  mov rax, SYS_ACCEPT
  syscall
  mov [rbp - VAR_SOCKET_FD], rax ; save fd

  ; Recive request
  mov rdi, [rbp - VAR_SOCKET_FD]
  mov rsi, [rbp - VAR_REQ_BUF]
  mov rdx, BUFFER_SIZE
  call sys_recvfrom

  ; Read the 5th caracter
  add rsi, 5
  mov al, [rsi]

  ; If it's ' ' then the request is "GET / ..."
  cmp al, 32 ; ' '
  je send_index_page

  ; If it's 'a' then the request is "GET /a ..."
  cmp al, 97 ; 'a'
  je send_join_page

  cmp al, 98 ; 'b'
  je send_state_page

  cmp al, 99 ; 'c'
  je send_guess_word_added

  ; Fall through to the default index response.

send_index_page:
  ; Send header.
  mov rdi, [rbp - VAR_SOCKET_FD]
  mov rsi, header_str
  mov rdx, header_len
  call sys_sendto

  ; Send response html.
  mov rsi, index_resp
  mov rdx, index_len
  call sys_sendto
  jmp finish_request

send_join_page:
  ; Send header.
  mov rdi, [rbp - VAR_SOCKET_FD]
  mov rsi, header_binary_str
  mov rdx, header_binary_len
  call sys_sendto

  ; Get player index.
  mov rsi, [rbp - VAR_REQ_BUF]
  add rsi, 7
  xor rax, rax
  mov al, [rsi]
  sub al, 48 ; '0'
  and al, 3 ; Make sure the only possible values are 0-3.
  mov r11, rax ; r11 what index the player wants to be.

  ; Load the game state.
  mov rbx, [rbp - VAR_GAME_STATE]
  add rbx, players ; rbx is now the begining of the player names
  mov rax, r11
  mov rcx, PLAYER_NAME_LEN
  mul rcx
  add rbx, rax ; rbx has been set to the start of where the player name should be.
  mov al, [rbx] ; bl is now the first character of the player name.
  cmp al, BYTE 32 ; ' '
  je player_slot_is_free_for_taking

  ; Send 'it didn't work'.
  mov rsi, constant_zero
  mov rdx, 1
  call sys_sendto
  jmp finish_request

player_slot_is_free_for_taking:
  ; Set the player name as `p0` for slot 0, ...
  mov rax, 'p'
  mov [rbx], al
  inc rbx
  mov rax, r11
  add rax, '0'
  mov [rbx], al

  ; Increase number of players.
  mov rbx, [rbp - VAR_GAME_STATE]
  add rbx, n_players_joined
  mov rax, [rbx]
  inc rax
  mov [rbx], rax
  cmp al, N_PLAYERS
  jne no_need_to_init_cards

  ; All the players have joined which means we have to set up the game now.

  ; Initialize the cards randomly.
  mov rbx, [rbp - VAR_GAME_STATE]
  add rbx, cards ; rbx is the start of the cards slot

  ; To generate unique cards, we need to keep track of the already generated
  ; ones. We use an array on the stack as a set. The array is 256 bytes long to
  ; avoid alignment issues. This will fit the 187 words the game has.
  ;
  ; If the newly generated word is `index`, then `[rsp + 1 + index]` stores
  ; whether or not the word has been generated previously. `+ 1` is used because
  ; `rsp` points at the current stack position which shouldn't be changed.
  sub rsp, 0xFF ; ends at `add rsp, 0xFF`
  mov rcx, 0xFF
  mov rsi, rsp
init_next_set_position: ; Initialize the whole set with zeros.
  inc rsi
  mov [rsi], BYTE 0
  dec rcx
  jz done_used_card_set_init
  jmp init_next_set_position

done_used_card_set_init:
  mov rcx, N_CARDS ; Count down to fill all.
  mov rsi, N_WORDS ; Used for modulo for random.

next_card:
  call random ; Get the random number.
  xor rdx, rdx
  div rsi ; Modulo. rdx is now a random word.
  ; We now need to check if it's unique.
  mov rdi, rsp
  inc rdi
  add rdi, rdx
  mov al, [rdi] ; al = [rsp + 1 + rdx], where rdx is the random word.
  cmp al, 0 ; 0 means it's unique, 1 means it's been used before.
  jne next_card ; Try another random number since this is used.
  ; rdx is indeed unique, so we can save it now.
  xor rax, rax
  inc rax
  mov [rdi], rax ; Save 1 in the set because this word has been generated.
  mov [rbx], dl ; Save the unique word on the card.
  inc rbx ; Jump over card type for now.
  inc rbx ; Now we're at the next card word.
  dec rcx
  jz finished_init_cards
  jmp next_card

finished_init_cards:
  ; Get rid of the used card set.
  add rsp, 0xFF

  ; Set the mover to 0.
  mov rbx, [rbp - VAR_GAME_STATE]
  add rbx, mover ; rbx points to the mover
  mov [rbx], BYTE 0

no_need_to_init_cards:

  ; Send 'it worked'.
  mov rdi, [rbp - VAR_SOCKET_FD]
  mov rsi, constant_one ; Pointer to 1.
  mov rdx, 1
  call sys_sendto
  jmp finish_request

send_state_page:
  ; Send header.
  mov rdi, [rbp - VAR_SOCKET_FD]
  mov rsi, header_binary_str
  mov rdx, header_binary_len
  call sys_sendto

  ; Send response data.
  mov rsi, [rbp - VAR_GAME_STATE]
  add rsi, sendable_state_start
  mov rdx, (sendable_state_end - sendable_state_start)
  call sys_sendto
  jmp finish_request

send_guess_word_added:
  ; Send header.
  mov rdi, [rbp - VAR_SOCKET_FD]
  mov rsi, header_binary_str
  mov rdx, header_binary_len
  call sys_sendto

  ; Get word count. The buffer has the format 'GET /c/2/month ...' where 2 is
  ; the number of words and 'month' is the suggestion word.
  mov rsi, [rbp - VAR_REQ_BUF]
  add rsi, 7 ; The number of words is at this index.
  xor rax, rax
  mov al, [rsi] ; Load the char.
  sub al, 48 ; Subtract '0' to get the number
  and al, 7 ; Make sure the only possible values are 0-7.
  mov rdi, [rbp - VAR_GAME_STATE] ; Load the game state.
  add rdi, current_word_count ; Advance to the current_word_count.
  mov [rdi], al ; Set current_word_count

  ; Now we need to copy the guess word.
  add rsi, 2 ; Advanced past '/' the the start of the word.
  mov rdi, [rbp - VAR_GAME_STATE] ; Load the game state.
  add rdi, current_word ; Advance to the current_word.
  mov rcx, CURRENT_WORD_LEN ; Se the maximum chars to copy.
  ; rdi is the destination.
  ; rsi is the source.
  ; rcx is the max copy length.
copy_next_char:
  mov al, [rsi]
  cmp al, 32 ; ' '
  je done_word_copy
  mov [rdi], al
  inc rsi
  inc rdi
  dec rcx
  jz done_word_copy
  jmp copy_next_char

done_word_copy:

  ; Send the response (always the same, just '\1')
  mov rdi, [rbp - VAR_SOCKET_FD]
  mov rsi, constant_one ; Pointer to 1.
  mov rdx, 1
  call sys_sendto
  jmp finish_request ; I guess this isn't needed.

finish_request:
  mov rdi, [rbp - VAR_SOCKET_FD]
  call sys_close

  jmp new_connection

exit_error:
  ; Close the socket.
  mov rdi, [listen_socket]
  call sys_close

  ; Write an error message.
  mov rdx, msg_error_len
  mov rsi, msg_error
  mov rdi, FD_STDOUT
  mov rax, SYS_WRITE
  syscall

  ; Bye bye, cruel world.
  mov rdi, -1
  mov rax, SYS_EXIT
  syscall

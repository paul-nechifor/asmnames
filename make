#!/usr/bin/env bash

set -euo pipefail

root=$(cd "$(dirname "${BASH_SOURCE[0]}")"; pwd)

debug=
unnecessary_blocks=(
    "--remove-section=.eh_frame"
    "--remove-section=.gnu.version"
    "--remove-section=.hash"
)

docs() {
    rm -fr docs
    js/node_modules/.bin/docco main.asm \
        -L <(echo '{".asm": {"name": "x86asm", "symbol": ";"}}')
    (cd docs; mv main.html index.html; sed -i 's/main.asm/ASM Names/' index.html)
}

tests() {
    cd tests
    poetry run pytest -vv test.py
}

main() {
    cd "$root"
    rm -fr asmnames
    yasm_args=(-f elf64 -a x86 main.asm -o main.o)
    if [[ $debug ]]; then
        yasm_args+=("--dformat=dwarf2")
    fi
    yasm "${yasm_args[@]}"
    ld main.o -o asmnames
    if [[ ! $debug ]]; then
        strip --strip-all --discard-all asmnames
        strip "${unnecessary_blocks[@]}" asmnames
    else
        docs
        tests
    fi
    rm -f main.o
}

while getopts d flag; do
    case "$flag" in
        d) debug=1;;
    esac
done

main
